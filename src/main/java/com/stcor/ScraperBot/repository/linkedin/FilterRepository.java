package com.stcor.ScraperBot.repository.linkedin;

import com.stcor.ScraperBot.Bots.model.linkedin.Filter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Lucas
 */
@Repository
public interface FilterRepository extends JpaRepository<Filter,String>{
}

package com.stcor.ScraperBot.repository.linkedin;

import com.stcor.ScraperBot.Bots.model.linkedin.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 *
 * @author Lucas
 */
@Repository
public interface ContactRepository extends JpaRepository<Contact,String>{
    Contact findByUrl(String url);
}

CREATE DATABASE  IF NOT EXISTS `scrapperbot` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `scrapperbot`;
-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: localhost    Database: scrapperbot
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `contact`
--

DROP TABLE IF EXISTS `contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contact` (
  `id` varchar(250) NOT NULL,
  `conected` datetime DEFAULT NULL,
  `created` date DEFAULT NULL,
  `img` varchar(5000) DEFAULT NULL,
  `job` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `url` varchar(5000) DEFAULT NULL,
  `connected` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact`
--

LOCK TABLES `contact` WRITE;
/*!40000 ALTER TABLE `contact` DISABLE KEYS */;
INSERT INTO `contact` VALUES ('cb6e855a-e990-4a33-938d-448f67b8eabd',NULL,'2020-09-15','https://media-exp1.licdn.com/dms/image/C4E03AQFwYHJ0F9xGaQ/profile-displayphoto-shrink_200_200/0?e=1605744000&v=beta&t=ILM6z5IeUw0aS3KLJ2-pot8L9eX2q1hXWMQTAQ-R1eo','Responsable de Recursos Humanos en Packing SA','Valeria Baez','https://www.linkedin.com/in/valeriabaezg/','2020-09-15 09:45:25'),('c773dc60-6597-43bf-99e5-912affcf205c',NULL,'2020-09-15','https://media-exp1.licdn.com/dms/image/C5603AQGtMQryZIyulQ/profile-displayphoto-shrink_200_200/0?e=1605744000&v=beta&t=uO1gUxup7OPVJXHc17PV_VsxV8S2_nPt2_kpHC3r8lQ','Lic. en Relaciones del Trabajo (UBA) - RRHH en TCba- Centro de diagnostico por Imagenes','Diego Ariel Pellicori','https://www.linkedin.com/in/diego-ariel-pellicori-9134b137/','2020-09-15 09:43:56');
/*!40000 ALTER TABLE `contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `filter`
--

DROP TABLE IF EXISTS `filter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `filter` (
  `id` varchar(250) NOT NULL,
  `created` date DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `filter`
--

LOCK TABLES `filter` WRITE;
/*!40000 ALTER TABLE `filter` DISABLE KEYS */;
INSERT INTO `filter` VALUES ('1',NULL,'human'),('2',NULL,'resources'),('3',NULL,'hr'),('4',NULL,'rh'),('5',NULL,'talent'),('6',NULL,'recruiter'),('7',NULL,'psicóloga'),('8',NULL,'selector'),('27',NULL,'búsqueda'),('10',NULL,'rrhh'),('11',NULL,'humanos'),('12',NULL,'recursos'),('13',NULL,'selectora'),('14',NULL,'select'),('15',NULL,'consultores'),('16',NULL,'humano'),('17',NULL,'acquisition'),('18',NULL,'búsquedas'),('20',NULL,'selección'),('21',NULL,'psicólogo'),('22',NULL,'relaciones'),('23',NULL,'recruiting'),('24',NULL,'psicología'),('28',NULL,'talento'),('29',NULL,'talentos'),('30',NULL,'talents'),('31',NULL,'hiring'),('32',NULL,'hiring!');
/*!40000 ALTER TABLE `filter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'scrapperbot'
--

--
-- Dumping routines for database 'scrapperbot'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-15 10:10:57
